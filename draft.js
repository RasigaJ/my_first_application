let foods = {
    "Biscuits": 10,
    "Chocolates": 5,
    'Dried Fruits': 40,
    "French Fries": 50,
    "Gulab Jamun": 100,
    "IceCream": 50,
    "Indian Snacks": 200,
    "Puffed Snacks": 40,
    "Popcorn": 10,
    "Roasted Nuts": 20,
    "Shots": 50,
    "Snack Bars": 20,
    "Vanilla Dessert": 20
}
let div = document.querySelector('.sub');
let btn = document.querySelector(".btn");
let product = document.querySelector('#browser')
let count = document.querySelector('#browser2');
let items = document.querySelector('.items');
let doneButton = document.querySelector('.button');
let draft = document.querySelector('.draft');
btn.addEventListener('click', (event) => {
    event.preventDefault();
    let div1 = document.createElement('div');
    div1.classList.add('design');
    let div2 = document.createElement('div');
    let h1 = document.createElement('h5');
    let h2 = document.createElement('h5');
    h1.textContent = `${product.value} - ${count.value}`;
    div1.classList.add('main');
    div2.classList.add('cart');
    div2.append(h1);
    div2.append(h2);
    div1.append(div2);
    document.body.appendChild(div1);
});
let totalAmount = 0;
let amount = 0;
let intCount = 0;
let amounts = 0

doneButton.addEventListener('click', () => {
    let gettingValue = document.getElementsByClassName('design');
    let newH5 = document.createElement('h5');
    for (let element of gettingValue) {
        let newH1 = document.createElement('h5');
        newH1.classList.add('H1')
        newH1.textContent = `${element.textContent}`;
        items.append(newH1);
        let newH2 = document.createElement('h5');
        newH2.classList.add('H2');
        newH2.textContent = `${element.textContent}`;
        let elementVAlue = element.textContent.split(' ')[0];
        if (elementVAlue == 'Biscuits') {
            amount = foods.Biscuits;
        } else if (elementVAlue == 'Chocolates') {
            amount = foods.Chocolates;
        } else if (elementVAlue == "Dried") {
            amount = foods["Dried Fruits"];
        } else if (elementVAlue == "French") {
            amount = foods["French Fries"]
        } else if (elementVAlue == "Gulab") {
            amount = foods["Gulab Jamun"];
        } else if (elementVAlue == "IceCream") {
            amount = foods.IceCream;
        } else if (elementVAlue == "Indian") {
            amount = foods["Indian Snacks"]
        } else if (elementVAlue == "Popcorn") {
            amount = foods.Popcorn;
        } else if (elementVAlue == "Puffed") {
            amount = foods["Puffed Snacks"];
        } else if (elementVAlue == "Roasted") {
            amount = foods["Roasted Nuts"];
        } else if (elementVAlue == "Shots") {
            amount = foods.Shots;
        } else if (elementVAlue == "Snack") {
            amount = foods["Snack Bars"];
        } else if (elementVAlue == "Vanilla") {
            amount = foods["Vanilla Dessert"];
        }
        let newH3 = document.createElement('h5');
        let newH4 = document.createElement('h5');
        intCount = +`${element.textContent.split("-")[1]}`;
        amounts = intCount * amount;
        totalAmount += amounts;
        newH4.textContent = `Single Price : ${amount}`;
        newH3.textContent = `Amount: ${amounts}`;
        newH5.textContent = `Grand Total: ${totalAmount}`;
        newH5.classList.add('total')
        items.append(newH4);
        items.append(newH3);
        draft.append(newH2);
    }
    items.append(newH5);
});
const myFun = () => {
    alert(`Your total bill is ₹${totalAmount}`)
}

