let baseUrl = "https://btc-heroku.herokuapp.com/"



var users = document.querySelector("#user");
users.addEventListener('keyup', () => {
    var u_times = document.querySelector('.u_times');
    var u_check = document.querySelector('.u_check');
    if (users.value.length == 0 || !users.value.includes("@gmail.com")) {
        users.style.border = '1px solid red';
        u_times.style.display = 'block';
        u_check.style.display = 'none';
    } else {
        users.style.border = '1px solid green';
        u_times.style.display = 'none';
        u_check.style.display = 'block';
    }
})
var password = document.querySelector("#password");
password.addEventListener('keyup', () => {
    var u_times = document.querySelector('.d');
    var u_check = document.querySelector('.e');
    if (password.value.length == 0 || password.value.length < 8) {
        password.style.border = '1px solid red';
        u_times.style.display = 'block';
        u_check.style.display = 'none';
    } else {
        password.style.border = '1px solid green';
        u_times.style.display = 'none';
        u_check.style.display = 'block';
    }
})

var con_pass = document.querySelector("#con-password");
con_pass.addEventListener('keyup', () => {
    var u_times = document.querySelector('.f');
    var u_check = document.querySelector('.g');
    if (con_pass.value.length == 0 || con_pass.value.length < 8 ) {
        con_pass.style.border = '1px solid red';
        u_times.style.display = 'block';
        u_check.style.display = 'none';
    } else {
        con_pass.style.border = '1px solid green';
        u_times.style.display = 'none';
        u_check.style.display = 'block';
    }
})

const validate = () => {
    if (users.value.length == 0 || !users.value.includes("@gmail.com")) {
        var p = document.querySelector('#error')
        p.style.display = 'block';
        p.innerHTML = "Please fill all!";
        // return false
    } else if (password.value.length == 0 || password.value.length < 8) {
        var p = document.querySelector('#error')
        p.style.display = 'block';
        p.innerHTML = "Please fill all!";
        // return false
    } else if (con_pass.value.length == 0 || con_pass.value.length < 8) {
        var p = document.querySelector('#error')
        p.style.display = 'block';
        p.innerHTML = "Please fill all!";
        // return false
    } else {
        // console.log("Successfully SignedUp");
        // window.location.replace("try.html");
        registerUser({username:users.value , password:password.value})
    }
}

const registerUser = async(value) =>{
   console.log(JSON.stringify(value))
   const response = await fetch(baseUrl +"users-register", {
    method: 'POST',
    body: JSON.stringify(value), // string or object
    headers: {
      'Content-Type': 'application/json'
    }
  });
  
  if(response.ok){
    window.location.replace("login.html")
  }
}


