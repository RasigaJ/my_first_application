let baseUrl = "https://btc-heroku.herokuapp.com/"

var users = document.querySelector("#user");
users.addEventListener('keyup', () => {
    var u_times = document.querySelector('.u_times');
    var u_check = document.querySelector('.u_check');
    if (users.value.length == 0 || !users.value.includes("@gmail.com")) {
        users.style.border = '1px solid red';
        u_times.style.display = 'block';
        u_check.style.display = 'none';
    } else {
        users.style.border = '1px solid green';
        u_times.style.display = 'none';
        u_check.style.display = 'block';
    }
})
var password = document.querySelector("#password");
password.addEventListener('keyup', () => {
    var u_times = document.querySelector('.d');
    var u_check = document.querySelector('.e');
    if (password.value.length == 0 || password.value.length < 8) {
        password.style.border = '1px solid red';
        u_times.style.display = 'block';
        u_check.style.display = 'none';
    } else {
        password.style.border = '1px solid green';
        u_times.style.display = 'none';
        u_check.style.display = 'block';
    }
})

const validate = () => {
    if (users.value.length == 0 || !users.value.includes("@gmail.com")) {
        var p = document.querySelector('#error')
        p.style.display = 'block';
        p.innerHTML = "Please fill all!";
        // return false
    } else if (password.value.length == 0 || password.value.length < 8) {
        var p = document.querySelector('#error')
        p.style.display = 'block';
        p.innerHTML = "Please fill all!";
        // return false
    } else {
        // console.log('login successfull')
       login({username:users.value,password:password.value})
    }

}


const login = async(value) =>{
    console.log(JSON.stringify(value))
    const response = await fetch(baseUrl +"users", {
     method: 'POST',
     body: JSON.stringify(value), // string or object
     headers: {
       'Content-Type': 'application/json'
     }
   });
   console.log(response,"response")
   if(response.ok){
    const myJson = await response.json()
   document.cookie = "accessToken" + "=" + myJson?.data?.accessToken
   window.location.replace("dashboard.html")
   }
   


   //how to store cookie --accessToken

   //redirect to home page

//    if(response.ok){
//      window.location.replace("login.html")
//    }
 }
 


