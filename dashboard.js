let foods = {
    "Biscuits": 10,
    "Chocolates": 5,
    'Dried Fruits': 40,
    "French Fries": 50,
    "Gulab Jamun": 100,
    "IceCream": 50,
    "Indian Snacks": 200,
    "Puffed Snacks": 40,
    "Popcorn": 10,
    "Roasted Nuts": 20,
    "Shots": 50,
    "Snack Bars": 20,
    "Vanilla Dessert": 20
}

let isEdit = false




let div = document.querySelector('.sub');
let btn = document.querySelector(".btn");
let product = document.querySelector('#browser')
let count = document.querySelector('#browser2');
let items = document.querySelector('.items');
let doneButton = document.querySelector('.button');
let draft = document.querySelector('.draft');
let userName = document.querySelector('.username');
let draftButton = document.querySelector('.close_create_modal');
let inputs = document.querySelector('.input');
let addProduct = document.getElementsByClassName("add_to-task_button")

console.log(addProduct)

let baseUrl = "https://btc-heroku.herokuapp.com/"

let productDetails

const resetFileds = () => {
    let getValue = document.getElementsByClassName("browsers")
    if (getValue.value != "") {
        getValue.value = "";
    }
    console.log(getValue?.value)
}
const addNewProduct = () => {
    let div1 = document.createElement('div');
    div1.classList.add('design');
    let div2 = document.createElement('div');
    let h1 = document.createElement('h5');
    let h2 = document.createElement('h5');
    h1.textContent = `${product.value} - ${count.value} - ₹${count.value * foods[product.value]}`;
    div1.classList.add('main');
    div2.classList.add('cart');
    div2.append(h1);
    div2.append(h2);
    div1.append(div2);
    // div1.style.display = 'none';
    // document.body.appendChild(div1);
    items.append(div1)
    resetFileds()
}

const newHit = () => { 
    console.log("close Modal") 
    let form_value = document.getElementsByClassName('input-box')
    form_value[0].reset()
    // userName.innerHTML="";
    let someone = document.querySelector(".items");
    someone.innerHTML="";

}
// btn.addEventListener('click', (event) => {
//     let input_value = document.getElementsByClassName("browsers")
//     event.preventDefault();
//     // product.innerHTML("")
//     // count.innerHTML("")
//     resetFileds()
// });
let totalAmount = 0;
let amount = 0;
let intCount = 0;
let amounts = 0
let newH2 = 0;
let productList = [];

let billList = []



const createProduct = async (value) => {

    console.log(value)
    const one = document.querySelector('.one');

    const bill = document.createElement('p');

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2Njg1NzczODB9.LfUpw7BmxZKX22lKnYvMuu1S6lyuRbcLUSMqzC3IvAE");
    myHeaders.append("Content-Type", "application/json");

    const response = await fetch(baseUrl + "billing", {
        method: 'POST',
        body: JSON.stringify(value), // string or object
        headers: myHeaders
    });
    if (response.ok) {
        const myJson = await response.json()
        billList.unshift(myJson?.data)
        one.innerHTML = ''
        closeModal()
        renderAllBils()

        //    document.cookie = "accessToken" + "=" + myJson?.data?.accessToken
        //    window.location.replace('try.html')
    }

}

const getAllBils = async () => {
    // closeModal()
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2Njg1NzczODB9.LfUpw7BmxZKX22lKnYvMuu1S6lyuRbcLUSMqzC3IvAE");
    myHeaders.append("Content-Type", "application/json");

    const response = await fetch(baseUrl + "billing", {
        method: 'GET', // string or object
        headers: myHeaders
    });
    console.log(response, "response")
    if (response.ok) {
        const myJson = await response.json()
        console.log(myJson)
        billList = myJson?.data


        renderAllBils()

        //    document.cookie = "accessToken" + "=" + myJson?.data?.accessToken
        //    window.location.replace('try.html')
    }
}

const renderAllBils = () => {
    // console.log(billList,"billList")
    const one = document.querySelector('.one');
    const bill = document.createElement('p');
    // console.log(renderTotalBill())
    one.innerHTML = `<div class="table-root">
    <div class="header">
        <div class='cs customer_name'>Customer Name</div>
        <div class='customer_name'>Total Amount</div>
        <div class='customer_name'></div>
        <div class='customer_name'></div>
    </div>
    <div class="table-body">
        ${renderTotalBill()}
    </div>
</div>`
    // `Customer Name :  <br>
    // Your total bill is `
    // bill.classList.add('bill');
    // one.append(bill);
}



const myFun = () => {
    let gettingValue = document.getElementsByClassName('design');
    // let newH5 = document.createElement('h5');
    for (let element of gettingValue) {
        // let newH1 = document.createElement('h5');
        // newH1.classList.add('H1')
        // newH1.textContent = `${element.textContent}`;
        // items.append(newH1);
        if (newH2 != element.textContent) {
            newH2 = document.createElement('h5');
            newH2.classList.add('H2');
            newH2.textContent = `${element.textContent}`;
            let elementVAlue = element.textContent.split(' ')[0];
            if (elementVAlue == 'Biscuits') {
                amount = foods.Biscuits;
            } else if (elementVAlue == 'Chocolates') {
                amount = foods.Chocolates;
            } else if (elementVAlue == "Dried") {
                amount = foods["Dried Fruits"];
            } else if (elementVAlue == "French") {
                amount = foods["French Fries"]
            } else if (elementVAlue == "Gulab") {
                amount = foods["Gulab Jamun"];
            } else if (elementVAlue == "IceCream") {
                amount = foods.IceCream;
            } else if (elementVAlue == "Indian") {
                amount = foods["Indian Snacks"]
            } else if (elementVAlue == "Popcorn") {
                amount = foods.Popcorn;
            } else if (elementVAlue == "Puffed") {
                amount = foods["Puffed Snacks"];
            } else if (elementVAlue == "Roasted") {
                amount = foods["Roasted Nuts"];
            } else if (elementVAlue == "Shots") {
                amount = foods.Shots;
            } else if (elementVAlue == "Snack") {
                amount = foods["Snack Bars"];
            } else if (elementVAlue == "Vanilla") {
                amount = foods["Vanilla Dessert"];
            }

            // let newH3 = document.createElement('h5');
            // let newH4 = document.createElement('h5');
            intCount = +`${element.textContent.split("-")[1]}`;
            amounts = intCount * amount;
            totalAmount += amounts;
            // newH4.textContent = `Single Price : ${amount}`;
            // newH3.textContent = `Amount: ${amounts}`;
            // newH5.textContent = `Grand Total: ${totalAmount}`;
            // newH5.classList.add('total')
            // items.append(newH4);
            // items.append(newH3);

            productList.push({
                "productName": `${element.textContent.split("-")[0]}`,
                "count": `${element.textContent.split("-")[1]}`,
                "price": `${amounts}`,

            })


        }
    }


    createProduct({
        "customerName": `${userName.value}`,
        "totalAmount": `${totalAmount}`,
        "productDetails": productList,
    })

}

    draftButton.addEventListener('click', () => {
        let gettingValue = document.getElementsByClassName('design');
        for (let element of gettingValue) {
            let newH2 = document.createElement('h5');
            newH2.classList.add('H2');
            newH2.textContent = `${element.textContent}`;
            draft.append(newH2);
        }
    })
   


const renderTotalBill = () => {
    let dataHtml = ""
    billList.forEach((_l) => {
        dataHtml = dataHtml + `<div class="table_infront">
                   <div class="customer_name">${_l.customerName}</div>
                   <div class="customer_name">₹${_l.totalAmount}</div>
                   <div class="customer_name"><div class='last-end' onClick="openDetails()"><div class="haii" onclick="myDetails('${_l._id}')">details</div></div>
                   <div class="customer_name delete_name"><div onclick="deleteProduct('${_l._id}')">Delete</div></div>
                   </div> 
                </div>`

    })

    return dataHtml
}

const myDetails = async (id) => {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2Njg1NzczODB9.LfUpw7BmxZKX22lKnYvMuu1S6lyuRbcLUSMqzC3IvAE");
    myHeaders.append("Content-Type", "application/json");

    const response = await fetch(baseUrl + "billing/" + id, {
        method: 'GET', // string or object
        headers: myHeaders
    });
    console.log(response, "response")
    if (response.ok) {
        const myJson = await response.json()
        renderProductDetails(myJson)
    }
}


const returnProductDetails = (product) => {
    let dataHtml = ""
    product.forEach((_l) => {
        dataHtml = dataHtml + `<div class="table_row">
               <div class="customer_name">${_l.productName}</div>
               <div class="customer_name">₹${_l.count}</div>
               <div class="customer_name">₹${_l.price}</div>
           </div>`
    })
    return dataHtml
}

const deleteProduct = async (id) => {

    var myHeaders = new Headers();
    myHeaders.append("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2Njg1NzczODB9.LfUpw7BmxZKX22lKnYvMuu1S6lyuRbcLUSMqzC3IvAE");
    myHeaders.append("Content-Type", "application/json");

    const response = await fetch(baseUrl + "billing/" + id, {
        method: 'DELETE', // string or object
        headers: myHeaders
    });
    if (response.ok) {
        myDelete(id)
    }
}


const myDelete = (id) => {
    let modal = document.getElementsByClassName("overlay")
    let upadatedlist = []
    billList.forEach((_l, ind) => {
        if (_l._id !== id) {
            upadatedlist.push(_l)
        }
    })
    billList = upadatedlist
    console.log(upadatedlist, billList)
    // modal[0].style.visibility = "hidden !important"
    renderAllBils()
}

let nameCs = 0;
function myValue(valueName) {

    nameCs = valueName;


}


const updateProduct = async (data, id) => {
    let isEditable = document.getElementsByClassName("edit_button")
    console.log(isEditable[0].innerHTML == "Edit", isEditable[0].innerHTML)
    if (isEditable[0].innerHTML == "Edit") {
        isEditable[0].innerHTML = "save"
        let enable_input = document.getElementsByClassName("user-input-field")
        enable_input[0].classList.remove("user-input-field")
        // enable_input[0].classList.add("enable_user_input")
    } else if (isEditable[0].innerHTML = "save") {
        isEditable[0].innerHTML == "save"
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2Njg1NzczODB9.LfUpw7BmxZKX22lKnYvMuu1S6lyuRbcLUSMqzC3IvAE");
        myHeaders.append("Content-Type", "application/json");


        const response = await fetch(baseUrl + "billing/" + id, {
            method: 'PUT', // string or object
            headers: myHeaders,
            body: JSON.stringify({ "customerName": `${nameCs}` })
        });
        const myJson = await response.json()
        console.log(myJson, "myJson")
        if (response.ok) {
            // myDelete(id)
            replaceProduct(id, myJson?.data)
            isEditable[0].innerHTML = "Edit"
        }
    }
}


const replaceProduct = (id, data) => {
    console.log(data, id)
    let upadatedlist = []
    billList.forEach((_l, ind) => {
        if (_l._id == id) {
            billList[ind] = data
        }
    })
    renderAllBils()
}


const renderisEdit = (edit) => {
    return edit

}


var lastDiv = document.createElement('div');
const renderProductDetails = (myJson) => {



    let editedProduct = {
        customerName: myJson?.data?.customerName,
    }

    let products = myJson?.data?.productDetails;

    let parentDiv = document.getElementsByClassName("product_details");
    var detailButton = document.querySelector('.product_details');


    lastDiv.classList.add('complete')
    lastDiv.innerHTML = `<h5 class="CName">Customer Name : <input class="user-input-field" onChange='myValue(this.value)'value=${editedProduct?.customerName} /></h5>
    <h5 class="TAmount">Total Amount : ${myJson?.data?.totalAmount}</h5>
    <div class="table-root">
    <div class="tailer">
        <div class='cn'>Product Name</div>
        <div class='tn'>Total Count</div>
        <div>Price</div>
    </div>
    <div class="table-body">
    ${returnProductDetails(products)}
    </div>
    <div class="del_button">
    <div href="#" class="edit_button" onclick="updateProduct('${editedProduct?.customerName}','${myJson?.data?._id}')">Edit</div>
    <div href="#" onClick="closeUpdateModal()" class="cancel_button" type="button">Cancel</div>
    </div>
    `;
    detailButton.append(lastDiv)
}

// const openModal = () => {

//     console.log(modal)
//     modal[0].style.display = "unset"
// }


let finalWork = document.querySelector('.last-end');

const openModal = () => {
    let modal = document.getElementsByClassName("create_modal")
    modal[0].classList.remove("close_modal")
    modal[0].classList.add("open_modal")
}

const closeModal = () => {
    let modal = document.getElementsByClassName("create_modal")
    modal[0].classList.add("close_modal")
    modal[0].classList.remove("open_modal")
}

// $('.close_create_modal').click(function() {
//     $('.create_modal').css({
//         'background-color': 'red',
//         'color': 'white',
//         'font-size': '44px'
//     });
// });


const openDetails = () => {
    console.log("cliked")
    let modal = document.getElementsByClassName("update_modal")
    // modal[0].classList.remove("close_modal")
    modal[0].classList.add("open_update_modal")
}

const closeUpdateModal = () => {
    let modal = document.getElementsByClassName("update_modal")
    let deails_modal = document.getElementsByClassName("product_details")
    modal[0].classList.remove("open_update_modal")
    modal[0].classList.add("close_modal")
    deails_modal[0].innerHTML = ""

}
